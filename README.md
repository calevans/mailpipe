# MailPipe

(c) 2022 E.I.C.C., Inc.

Released under  the MIT license

This is written to be a generic email to API pipe. It can be used by any MTA that can pipe a message to an external script. It will look up the recipient address in the `aliases.php` and if it finds a hit, it will POST the email to the API endpoint specified. Credentials can also be specified in the alias entry.

## Installation
1. Clone this into your mail server
2. Run `composer install`
3. Create `config/config.php`
4. Create `config/aliases.php`
5. Add an alias to your mail system that pipes emails to the script. `| /path/to/script/console.sh`

## `aliases.php`

`aliases.php` creates and returns a PHP array of the aliases that this script will process. If an email is sent to it that is to an address that is not in this array, it will throw an exception and stop processing.

An entry for `aliases.php` looks like this.

```php
return [
  'bob@example.com' => [
    'type' => 'restpost',
    'url' => 'https://example.com/mailscript.php',
    'auth' => [
      'value' => 'Bearer xyz'
    ],
    'validSenders' => [
    ],
  ],
  'ted@example.com' => [
    'type' => 'wpmediapost',
    'url' => 'https://example.com/wp-json/wp/v2/media',
    'auth' => [
      'value' => 'Basic Ym9iOmJvYnNwYXNzd29yZF9iYXNlNjRlbmNvZGVk'
    ],
    'validSenders' => [
      'bob@example.com'
    ],
  ],
  'alice@example.com' => [
    'type' => 'restpost',
    'url' => 'htttps://example.com/no-auth-required.php',
    'auth' => [
      'value' => ''
    ],
    'validSenders' => [
      'bob@example.com'
      'ted@example.com'
    ],
  ]
];
```
### Auth

If using Basic auth, make sure that the value stored is the base64 encoded value of username:password.

### `validSenders`

Populate the array `validSenders` with the email addresses that can send to this
alias. If the array is empty then anyone can send to it. If it's not empty then
the email address has to be in the array or the email will be rejected.

If you are validating the sender's email address in the webhook being called
then leave this array empty.

# `config.php`

Copy `config/config.sample` over and adjust as necessary.

# Testing

There are 2 ways you can test this in production.

1. `cat data/email.dev.email | app/console.sh` That will pump a valid email through the dev system.
2. Send a picture via email. Now Google loves to hold emails if they see the same thing going over and over again. So, I generally use one of my domains already on SiteGround. Just make sure and add the email address to `config/aliases.php`.

