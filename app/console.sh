#!/usr/bin/env php
<?php
declare(strict_types=1);

use Symfony\Component\Console\Application;

$appPath = realpath(dirname(__FILE__) . '/../') . '/';
require_once $appPath . 'vendor/autoload.php';

use MailPipe\Commands\EmailPipeCommand;

$container = require_once realpath(dirname(__FILE__)) . '/bootstrap.php';
$app = new Application('MailPipe', '1.0.0');
$app->container = $container;

$pipe = new EmailPipeCommand();

$app->addCommands(
  [
    $pipe
  ]
);

$app->setDefaultCommand($pipe->getName());
try {
  $app->run();
} catch (\Throwable $e) {
  error_log($e->getMessage()??'NO MESSAGE');
}

