<?php

declare(strict_types=1);

namespace MailPipe\Models;

use ZBateson\MailMimeParser\Message;
use ZBateson\MailMimeParser\Message\Part\MimePart;

/**
 * Represents a post to a WP Media Rest Endpoint
 *
 * This takes a message in the __invoke() and builds the payload and then
 * executes the POST. Then it executes a PATCH to send any extra data.
 *
 * This is ONLY for messages with media attachments. It should check the
 * attachment's Content-Type and throw an exception on any one that does not
 * match the whitelist.
 */
class WPMediaPost extends AbstractPost
{

    public function __invoke(
        Message $message,
        array $alias,
        bool $debug = false
    ) : void {
        $this->debug = $debug;
        $this->alias = $alias;

        $this->initializePayload($debug);
       /*
        * Attachments is really all we care about.
        * WordPress wants one attachment per POST.
        */
        for ($lcvA = 0; $lcvA < $message->getAttachmentCount(); $lcvA++) {
            $this->sendMediaToWordPress(
                $debug,
                $message,
                $message->getAttachmentPart($lcvA)
            );
        }
    }

    /*
     *
     */
    protected function sendMediaToWordPress(
        bool $debug,
        Message $message,
        MimePart $attachment
    ) : object {

        $this->initializePayload();
        $this->addPart(
            'file',
            $attachment->getContentStream(),
            [
            'filename' =>  $attachment->getFilename(),
            'headers'  => [
              'Content-Type' => $attachment->getContentType()
            ]
            ]
        );

        $response = $this->client->post(
            $this->alias['url'] . '/wp-json/wp/v2/media',
            $this->payload
        );
        return json_decode($response->getBody()->getContents());
    }

    /**
     * Fetch the ids of
     * e.g given that holiday is category id 7, get a list of all categories
     * that are under holiday.
     */
    protected function fetchTaxonomyFamily(
        int $value,
        string $filter
    ) : array {

        switch (trim($filter)) {
            case 'tags':
                $display = 'Tags';
                $endpoint = 'tags';
                break;
            case 'categories':
                $display = 'Categories';
                $endpoint = 'categories';
                break;
            default:
                throw new \Exception($filter . ' is not a valid taxonomy type.');
        }

        $response = $this->client->request(
            'GET',
            $this->alias['url'] . 'wp-json/wp/v2/' . $endpoint . '?parent=' . $value,
            [
                'headers' => [
                'Accept'     => 'application/json',
                'Authorization'=> $this->alias['auth']['value']
            ],
            'debug' => $this->debug,
            'http_errors' => $this->debug
            ]
        );
        $decoded = json_decode($response->getBody()->getContents());

        if (count($decoded) < 1) {
            throw new \Exception($value . ' is not a valid ' . $display);
        }

        $returnValue = array_map(
            function ($value) {
                return $value->id;
            },
            $decoded
        );
        return $returnValue;
    }

  /**
   * Fetch the id of a specific taxonomy item
   */
    public function fetchTaxonomyId(
        string $value,
        string $filter
    ) : int {

        switch (trim($filter)) {
            case 'tags':
                $display = 'Tags';
                $endpoint = 'tags';
                break;
            case 'categories':
                $display = 'Categories';
                $endpoint = 'categories';
                break;
            default:
                throw new \Exception($filter . ' is not a valid taxonomy type.');
        }

        $response = $this->client->request(
            'GET',
            $this->alias['url'] . 'wp-json/wp/v2/' . $endpoint . '?search=' . $value,

            [
                'headers' => [
                'Accept'     => 'application/json',
                'Authorization'=> $this->alias['auth']['value']
                ],
                'debug' => $this->debug,
                'http_errors' => $this->debug,
            ]
        );
        $decoded = json_decode($response->getBody()->getContents());

        if (count($decoded) < 1) {
            return 0;
        }
        return $decoded[0]->id;
    }

}
