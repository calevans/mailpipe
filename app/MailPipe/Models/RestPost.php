<?php

declare(strict_types=1);

namespace MailPipe\Models;

use ZBateson\MailMimeParser\Message;

  /*
   * Represents a Generic REST POST
   *
   * This takes a message in the __invoke() and builds the payload and then
   * executes the POST.
   *
   * This should work for any proper REST endpoint. It is up to the Endpoint to
   * know how to deal with the attachments and their types.
   */
class RestPost extends AbstractPost
{

    public function __invoke(
        Message $message,
        array $alias,
        bool $debug = false
    ): void {
        $fromEmail = $message->getHeader('From')->getEmail();
        $toEmail = $message->getHeader('To')->getEmail();

        $this->initializePayload($debug);

        foreach ($message->getAllHeaders() as $header) {
            $this->addPart($header->getName(), $header->getValue());
        }

        $this->addPart('BodyHtml', $message->getHtmlContent());
        $this->addPart('BodyText', $message->getTextContent());

        $this->addAttachments($message);

        $this->setAuth($alias);

        try {
            $this->client->post($alias['url'], $this->payload);
        } catch (\Throwable $e) {
            echo $e->getResponse()->getBody()->getContents();
            die();
        }
    }
}
