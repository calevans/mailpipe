<?php

declare(strict_types=1);

namespace MailPipe\Models;

use MailPipe\Interfaces\Post;
use GuzzleHttp\Client;
use ZBateson\MailMimeParser\Message;

abstract class AbstractPost implements Post
{
    protected array $payload = [];
    protected ?Client $container = null;
    protected bool $debug = false;
    protected array $alias = [];

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    abstract public function __invoke(
        Message $message,
        array $alias,
        bool $debug = false
    ) : void;

  /*
   * Initialize the array
   */
    protected function initializePayload(): void
    {
        $this->payload =  [
        'debug' => $this->debug,
        'multipart' => [],
        'headers' => []
        ];
        $this->setAuth();
    }

   /*
    * Add a record to the payload multipart array.
    * If the name already exists, append it, otherwise, create a new entry.
    * New entries can also have an optional array of other parameters merged in.
    * These are headers, etc.
    */
    protected function addPart(string $key, $value, array $optional = []): void
    {

        foreach ($this->payload['multipart'] as $currentKey => $currentValue) {
            if ($currentValue['name'] === $key) {
                 $this->payload['multipart'][$currentKey]['contents'] .=  "\n" . $value;
                 return;
            }
        }

        $results = [
        'name' => $key, // This has to be 'file' for WordPress...INORITE?!?
        'contents' => $value
        ];

        $this->payload['multipart'][] = array_merge($results, $optional);
    }

    protected function setAuth(): void
    {
        if (
            isset($this->alias['auth']['value']) &&
            !empty(isset($this->alias['auth']['value']))
        ) {
            $this->payload['headers']['Authorization'] = $this->alias['auth']['value'];
        }
    }

    public function addAttachments(Message $message): void
    {
      /*
       * Attachments
       */
        for ($lcvA = 0; $lcvA < $message->getAttachmentCount(); $lcvA++) {
            $attachment = $message->getAttachmentPart($lcvA);
            $this->addPart(
                $attachment->getFilename(),
                $attachment->getContentStream(),
                [
                'filename' =>  $attachment->getFilename(),
                'headers'  => [
                'Content-Type' => $attachment->getContentType()
                ]
                ]
            );
        }
    }
}
