<?php

declare(strict_types=1);

namespace MailPipe\Models;

class Alias
{
    protected $aliases = [];

    public function __construct(array $aliases)
    {
        $this->aliases = $aliases;
    }

    /**
     * Given an email address, this will return the alias array
     */
    public function getAlias(string $fromEmail, string $toEmail): array
    {
        $toEmail = trim(strtolower($toEmail));
        $alias = null;
        /*
         *Check for exact match
         */
        if (isset($this->aliases[$toEmail])) {
            $alias = $this->aliases[$toEmail];
        }

        /*
         * Check for wildcard *@example.com
         */
        if (is_null($alias)) {
            $piecesParts = explode('@', $toEmail);

            if (count($piecesParts) !== 2) {
                throw new \Exception($toEmail . ' is malformed.');
            }

            $wildcard = "*@" . $piecesParts[1];

            if (! isset($this->aliases[$wildcard])) {
                throw new \Exception($toEmail . ' is not known to us.');
            }

            $alias = $this->aliases[$wildcard];
        }

        if (
            count($alias['validSenders'] ?? []) > 0 and
            !in_array($fromEmail, $alias['validSenders'])
        ) {
            throw new \Exception($fromEmail . ' cannot send email to ' . $toEmail);
        }

        return $alias;
    }
}
