<?php

/**
 * @todo implement a real logger instead of passing around the output.
 */

declare(strict_types=1);

namespace MailPipe\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use ZBateson\MailMimeParser\MailMimeParser;
use ZBateson\MailMimeParser\Message;
use ZBateson\MailMimeParser\MimePart;
use MailPipe\Models\Alias;

/**
 * EmailPipeCommand
 *  Main Command of the MailPipe system. This reads STDIN , processed the input,
 * finds which webhook to call, and passes the processed email to the webhook.
 */
class EmailPipeCommand extends Command
{
    protected $debug = false;

  /**
   * Called by the application, this method sets up the command.
   */
    protected function configure()
    {
        $definition = [
        ];

        $this->setName('pipe')
           ->setDescription('Takes an email from STDIN and pass it to a webhook.')
           ->setDefinition($definition)
           ->setHelp('Read an email from STDIN, process it and prepare it as ' .
                     'a JSON payload. Then call a webhook associated with ' .
                     'the TO address and POST the JSON payload.');
        return;
    }

  /**
   * Main body of this command
   */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->debug = $output->isDebug();
        $this->getApplication()->container['output'] = $output;

        $message = Message::from($this->getRawMessage());
        $fromEmail = $message->getHeader('From')->getEmail();
        $toEmail = $message->getHeader('To')->getEmail();

        $alias = $this->getApplication()->container['alias']
          ->getAlias($fromEmail, $toEmail);
          $this->getApplication()->container[$alias['type']](
            $message,
            $alias,
            $this->debug
        );

        $this->output->writeln('Done', OutputInterface::VERBOSITY_DEBUG) ;
        return 0;
    }

    /**
     * Pull the message out of STDIN
     */
    protected function getRawMessage(): string
    {
        $returnValue = '';

        $handle = fopen('php://STDIN', 'r');
        while ($bytes = fgets($handle)) {
            $returnValue .= $bytes;
        }
        fclose($handle);

        return $returnValue;
    }
}
