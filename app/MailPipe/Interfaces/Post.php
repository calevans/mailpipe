<?php

declare(strict_types=1);

namespace MailPipe\Interfaces;

use ZBateson\MailMimeParser\Message;
use MailPipe\Models\Alias;
use Pimple\Container;
use GuzzleHttp\Client;

interface Post
{
    public function __construct(Client $client);

    public function __invoke(
        Message $message,
        array $alias,
        bool $debug = false
    ): void;
}
