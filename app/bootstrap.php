<?php

declare(strict_types=1);

use Pimple\Container;
use GuzzleHttp\Client;

$container = new Container();
$configFunc = require_once $appPath . "config/config.php";
$config = $configFunc($appPath);

$container['guzzle'] = new Client();
$container['config'] = $config;

$container['alias'] = function ($c) {
    $aliases = include $c['config']['appPath'] . "config/aliases.php";
    return new MailPipe\Models\Alias($aliases);
};

$container['wpmediapost'] = function ($c) {
    return new MailPipe\Models\WPMediaPost($c['guzzle']);
};

$container['restpost'] = function ($c) {
    return new MailPipe\Models\RestPost($c['guzzle']);
};

$container['momsframepost'] = function ($c) {
    return new MailPipe\Models\MomsFramePost($c['guzzle']);
};

return $container;
